# Register w/ Attend

This is a starting platform by which you can build your perfect event
registration pages.

Please add on and extend everything that's here. The short version of
the MPL is that if you have changed one of these files, you need to
publish those changes. Really, though, just extend these to include your
own files; you don't have to push those back.

First, just run `yarn` to get packages installed. Then, check out the
`scripts` object in `package.json`. You can run `yarn run watch`, for
example, to build the development version w/ the webpack watch loop.

Next, to serve up what's here, try out `http-server`:

```
npm install -g http-server
http-server build/
```

This is a React application, which uses Redux and other standard tools.
We think this is a killer way to get up and going, but you are also free
to create similar systems that use some of the basic logic.
