const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const TARGET = 'build';

module.exports = (env = {}) => new Object({
	entry: {
		index: './src/',
		register: './src/register/',
		confirmation: './src/confirmation/'
	},
	output: {
		filename: (() => {
			if (env.production) return '[name].[chunkhash].js';
			return '[name].js';
		})(),
		path: path.resolve(__dirname, TARGET)
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				use: 'babel-loader'
			},
			{
				test: /\.css$/,
				use: [ 'style-loader', 'css-loader' ]
			}
		]
	},
	plugins: [
		// https://webpack.js.org/guides/caching/#get-filenames-from-webpack-compilation-stats
		function() {
			if ( ! env.production) return false;

			this.plugin('done', function(stats) {
				require('fs').writeFileSync(
					path.join(__dirname, TARGET, 'chunks-manifest.json'),
					JSON.stringify(stats.toJson()['assetsByChunkName']));
			});
		},
		new webpack.optimize.CommonsChunkPlugin({
			name: 'common'
		}),
		new HtmlWebpackPlugin({
			title: 'Landing Page',
			template: 'src/template.ejs',
			chunks: [ 'common', 'index' ]
		}),
		new HtmlWebpackPlugin({
			title: 'Register w/ Attend',
			template: 'src/template.ejs',
			filename: 'register/index.html',
			chunks: [ 'common', 'register' ]
		}),
		new HtmlWebpackPlugin({
			title: 'Thank You!',
			template: 'src/template.ejs',
			filename: 'confirmation/index.html',
			chunks: [ 'common', 'confirmation' ]
		})
	]
});
