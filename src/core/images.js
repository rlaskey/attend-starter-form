import React from 'react';
import css from './images.css';

export const ResponsiveImage = (props) => (<div
className="image-wrapper"
style={{
	paddingBottom: (100 * props.heightOverWidth) + '%'
}}>
	<img
		className="image-wrapped"
		src={props.src}
		/>
</div>);
