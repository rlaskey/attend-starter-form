import React from 'react';

import { ResponsiveImage } from './images';

const heroUrl = require('file-loader!../../assets/images/boston-hero.jpg');

export const HeroImage = () => (<ResponsiveImage
	heightOverWidth={500 / 1280}
	src={`/${heroUrl}`} />
);
