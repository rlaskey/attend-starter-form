import React from 'react';

export class Input extends React.Component {
	constructor(props) {
		super(props);

		this.state = { value: this.props.value };

		this.name = this.props.name || 'Field';
		this.field = this.props.field ||
			this.name.toLowerCase().replace(' ', '_');

		['onChange'].map((fn) => {
			this[fn] = this[fn].bind(this);
		});
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.value !== nextProps.value) {
			this.setState({ value: nextProps.value });
		}
	}

	onChange(e) {
		if ( ! this.props.onChange) {
			this.setState({ value: e.target.value });
			return;
		}

		this.props.onChange(this.field, e.target.value);
	}

	render() {
		return (<label className={this.props.className || 'block'}>
			{this.name}
			:
			{' '}
			{this.props.required &&
				<span className="text-danger">*</span>
			}
			<input type={this.props.type || 'text'}
				name={this.field}
				value={this.state.value}
				required={this.props.required || false}
				placeholder={this.props.placeholder}
				onChange={this.onChange} />
		</label>);
	}
};
