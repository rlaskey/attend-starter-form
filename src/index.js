import './vendor.js';

import React from 'react';
import { render } from 'react-dom';

import { HeroImage } from './core/hero-image';

import css from './style.css';

const Landing = () => (<div>
<HeroImage />

<main>
	<h1>Landing Page for Attend Event</h1>

	<p>List of Speakers / Agenda / Logos.</p>
	<p><a href="register/">Register Now!</a></p>
</main>
</div>);

render(<Landing />, document.getElementById('root'));
