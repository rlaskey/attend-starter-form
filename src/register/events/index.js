import React from 'react';
import { connect } from 'react-redux';

import Event from './event';

const Events = (props) => {
	if ( ! props.isActiveRoute) return null;

	return (<section id="events">
		{props.events.map(event => (<Event
			key={event.id}
			event={event}
			isSelectable={props.events.length > 1}
		/>))}
	</section>);
}

const mapStateToProps = state => new Object({
	events: state.events,
	isActiveRoute: state.activeRoute === 'EVENTS'
});

export default connect(mapStateToProps)(Events);
