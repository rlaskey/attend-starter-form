import React from 'react';
import { connect } from 'react-redux';

import { toggleEvent } from '../actions/event';
import Registration from './registration';

const Event = (props) => (<article>
{props.isSelectable && <button type="button"
	className={(props.event.isSelected ? '' : 'bg-danger ') + 'align-left'}
	onClick={props.toggle}>
	{props.event.isSelected ? '\u00D7' : '+' }
</button>}
	<h2>{props.event.name}</h2>

	<blockquote>{props.event.description}</blockquote>

{props.event.isSelected && props.registrations &&
<section className="registrations">
	{props.registrations.map(registration => (<Registration
		key={registration.uuid}
		event={props.event}
		registration={registration}
	/>))}
</section>}
</article>);

const mapStateToProps = (state, ownProps) => new Object({
	registrations: state.registrations[ownProps.event.id] || []
});

const mapDispatchToProps = (dispatch, ownProps) => new Object({
	toggle: (name, value) => dispatch(toggleEvent(ownProps.event.id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Event);
