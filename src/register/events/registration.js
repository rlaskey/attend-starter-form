import React from 'react';
import { connect } from 'react-redux';

import { Input } from '../../core/forms';
import { updateRegistration } from '../actions/registration';

const Registration = (props) => (<article>
{props.primaryFields.map(field => (
	<Input key={field.field}
		name={field.name}
		field={field.field}
		onChange={props.onChange}
		required={field.is_required}
		value={props.registration[field.field]} />
))}
{props.event.custom_fields && props.event.custom_fields.map(field => (
	<Input key={field.id}
		name={field.name}
		field={`cf_${field.id}`}
		onChange={props.onChange}
		required={field.is_required}
		value={props.registration[`cf_${field.id}`]} />
))}
</article>);

const mapStateToProps = state => new Object({
	primaryFields: state.primaryRegistration.fields
});

const mapDispatchToProps = (dispatch, ownProps) => new Object({
	onChange: (name, value) => {
		dispatch(updateRegistration(
			{
				event_id: ownProps.event.id,
				uuid: ownProps.registration.uuid,
			},
			{ name, value }
		));
	}
});

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
