const validRoutes = [
	'EVENTS',
	'PRIMARY_REGISTRATION'
];

export default (state = 'EVENTS', action) => {
	if (action.type !== 'CHANGE_ROUTE') return state;

	if (validRoutes.indexOf(action.route) === -1) return state;
	return action.route;
}
