import { combineReducers } from 'redux';

import activeRoute from './active-route';
import events from './events';
import primaryRegistration from './primary-registration';
import registrations from './registrations';

const combinedReducer = combineReducers({
	activeRoute,
	events,
	primaryRegistration,
	registrations
});

export default combinedReducer;
