const addEvents = (events) => events.map(
	event => Object.assign({}, event, { isSelected: true })
);

const toggleEvent = (state, eventId) => {
	return state.map(event => {
		if (event.id !== eventId) return event;

		return Object.assign({}, event, {
			isSelected: event.isSelected ? false : true
		});
	});
};

export default (state = [], action) => {
	switch (action.type) {
		case 'ADD_EVENTS': return addEvents(action.events);
		case 'TOGGLE_EVENT': return toggleEvent(state, action.eventId);
		default:
			return state;
	}
}
