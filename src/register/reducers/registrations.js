import { v4 } from 'node-uuid';

const blankCustomFields = (event) => {
	if ( ! event.custom_fields) return {};

	let blanks = {};

	event.custom_fields.map(custom_field => {
		blanks[`cf_${custom_field.id}`] = '';
	});

	return blanks;
};

const newRegistration = (event, primaryRegistration) => Object.assign(
	{ uuid: v4() },
	blankCustomFields(event),
	primaryRegistration
);

const setupRegistrationSlots = (state, events, primaryRegistration) => {
	let newState = {};
	events.map(event => {
		if ( ! state[event.id]) {
			newState[event.id] = [newRegistration(event, primaryRegistration)];
		}
	});

	return Object.assign({}, state, newState);
};

const fillWithPrimary = (state, events, primaryRegistration) => {
	let newState = {};
	events.map(event => {
		newState[event.id] = state[event.id].map(registration => {
			let filledRegistration = {};
			for (const field in registration) {
				if (registration[field]) continue;
				if ( ! primaryRegistration[field]) continue;
				filledRegistration[field] = primaryRegistration[field];
			}

			return Object.assign({}, registration, filledRegistration);
		});
	});

	return newState;
};

const updateRegistration = (state, registration, data) => {
	const updated = state[registration.event_id].map(registration => {
		if (registration.uuid !== registration.uuid) return registration;

		return Object.assign({}, registration, {
			[data.name]: data.value
		});
	});

	return Object.assign({}, state, {
		[registration.event_id]: updated
	});
};

export default (state = {}, action) => {
	switch (action.type) {
		case 'SETUP_REGISTRATION_SLOTS': return setupRegistrationSlots(
			state, action.events, action.primaryRegistration
		);
		case 'FILL_WITH_PRIMARY': return fillWithPrimary(
			state, action.events, action.primaryRegistration
		);
		case 'UPDATE_REGISTRATION': return updateRegistration(
			state, action.registration, action.data
		);
		default: return state;
	}
}
