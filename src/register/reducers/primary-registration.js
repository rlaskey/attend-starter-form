const primaryRegistrationDefault = {
	data: {},
	relevant: false,
	fields: [
		{
			name: 'First Name',
			field: 'first_name',
			is_required: true
		},
		{
			name: 'Last Name',
			field: 'last_name',
			is_required: true
		},
		{
			name: 'Email',
			field: 'email',
			is_required: true,
			type: 'email'
		}
	],
	errors: []
};

const addFields = (state, newFields) => {
	let freshResult = state.fields.slice();

	newFields.map(field => {
		freshResult.push({
			name: field.name,
			field: `primary_field_${field.id}`,
			is_required: field.is_required,
			type: field.type
		});
	});

	return Object.assign({}, state, { fields: freshResult });
};

const setup = (state) => {
	const newData = {};
	state.fields.map(field => {
		newData[field.field] = '';
	});

	return Object.assign({}, state, {
		data: Object.assign({}, state.data, newData),
		relevant: true
	});
}

const update = (state, newData) => {
	return Object.assign({}, state, {
		data: Object.assign({}, state.data, newData)
	});
};

const save = (state) => {
	const badFields = state.fields.filter(field => {
		if ( ! field.is_required) return false;

		if (state.data[field.field]) return false;
		return true;
	}).map(field => `${field.name} is required.`);

	return Object.assign({}, state, { errors: badFields });
};

export default (state = primaryRegistrationDefault, action) => {
	switch (action.type) {
		case 'ADD_PRIMARY_FIELDS': return addFields(state, action.fields);
		case 'UPDATE_PRIMARY_REGISTRATION': return update(state, action.data);
		case 'SETUP_PRIMARY_REGISTRATION': return setup(state);
		case 'SAVE_PRIMARY_REGISTRATION': return save(state);
		case 'NO_PRIMARY_REGISTRATION':
			return Object.assign({}, state, {
				data: {},
				relevant: false
			});
		default:
			return state;
	}
}
