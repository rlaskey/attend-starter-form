export const setupRegistrationSlots = (
	events, primaryRegistration
) => new Object({
	type: 'SETUP_REGISTRATION_SLOTS',
	events,
	primaryRegistration
});

export const fillWithPrimary = (events, primaryRegistration) => new Object({
	type: 'FILL_WITH_PRIMARY',
	events,
	primaryRegistration
});
