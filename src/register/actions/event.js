export const toggleEvent = (eventId) => new Object({
	type: 'TOGGLE_EVENT',
	eventId
});
