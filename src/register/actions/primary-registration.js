import { changeRoute } from './';
import { fillWithPrimary } from './registrations';

export const addPrimaryFields = (fields) => new Object({
	type: 'ADD_PRIMARY_FIELDS',
	fields
});

export const setupPrimaryRegistration = () => new Object({
	type: 'SETUP_PRIMARY_REGISTRATION'
});

export const updatePrimaryRegistration = data => new Object({
	type: 'UPDATE_PRIMARY_REGISTRATION',
	data
});

const savePrimaryRegistration = () => new Object({
	type: 'SAVE_PRIMARY_REGISTRATION'
});

export const continuePrimaryRegistration = () => (dispatch, getState) => {
	dispatch(savePrimaryRegistration());

	if ( ! getState().primaryRegistration.errors.length) {
		dispatch(fillWithPrimary(
			getState().events,
			getState().primaryRegistration.data
		));
		dispatch(changeRoute('EVENTS'));
	}
};
