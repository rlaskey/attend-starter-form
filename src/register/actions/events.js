import { setupRegistrationSlots } from './registrations';

const addEvents = (events) => new Object({
	type: 'ADD_EVENTS',
	events
});

export const setupEvents = (events) => (dispatch, getState) => {
	dispatch(addEvents(events));

	if (events.length) {
		dispatch(setupRegistrationSlots(
			events, getState().primaryRegistration.data
		));
	}
};
