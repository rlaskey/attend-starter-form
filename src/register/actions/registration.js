export const updateRegistration = (registration, data) => new Object({
	type: 'UPDATE_REGISTRATION',
	registration,
	data
});
