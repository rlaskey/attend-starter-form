export const changeRoute = (route) => new Object({
	type: 'CHANGE_ROUTE',
	route
});
