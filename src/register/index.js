import '../vendor.js';

import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';

import css from '../style.css';
import combinedReducer from './reducers';

import Register from './containers/register';

const store = createStore(
	combinedReducer,
	applyMiddleware(ReduxThunk)
);


render(
	<Provider store={store}>
		<Register />
	</Provider>,
	document.getElementById('root')
);
