import React from 'react';
import { connect } from 'react-redux';

import { setupEvents } from '../actions/events';
import { changeRoute } from '../actions/';
import {
	addPrimaryFields, setupPrimaryRegistration
} from '../actions/primary-registration';

import Events from '../events/';
import PrimaryRegistration from '../primary-registration/';
import { HeroImage } from '../../core/hero-image';

import FakeRequest from '../stub-requests/forms/multi-event';

class Register extends React.Component {
	componentDidMount() {
		this.getFormInfo().then(this.processFormInfo.bind(this));
	}

	getFormInfo() {
		return Promise.resolve(FakeRequest);
	}

	processFormInfo(info) {
		if (info.primary_fields.length) {
			this.props.dispatch(addPrimaryFields(info.primary_fields));
		}

		if (info.events) {
			if (info.events.length > 1) {
				this.props.dispatch(setupPrimaryRegistration());
				this.props.dispatch(changeRoute('PRIMARY_REGISTRATION'));
			}

			this.props.dispatch(setupEvents(info.events));
		}
	}

	render() {
		return (<form>
			<HeroImage />

			<main>
				<h1>Register w/ Attend.</h1>

				<PrimaryRegistration />
				<Events />
			</main>
		</form>);
	}
};

export default connect()(Register);
