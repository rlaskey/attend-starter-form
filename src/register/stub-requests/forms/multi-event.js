export default new Object({
	form: {
		id: 1,

		registration_types: [],

		name: 'Register w/ Attend: Test Form',
		description: 'Paragraph Text',
		logo_url: 'path/to/image',

		wavier: 'By registering for this event, you agree...',

		form_unique_value: 'ABC-123',

		time_display: 'Friday, June 30, 2017, 2pm',

		donations_on: true,
		donation_text: 'Please consider giving',
		promos_on: true,

		collect_unique_value_on: true,
		is_email_required: true,

		open_on: true,
		multi_event_selection_limit: null,

		currency_code: 'USD',
		language: 'en',
		title_font: 'Open Sans',
		body_font: 'Fira Sans',

		sharing_on: true,
		twitter_text: 'Join me at Test Form',
		facebook_text: 'Join me at Test Form',
		linkedin_text: 'Join me at Test Form'
	},
	events: [
		{
			id: 1,

			custom_fields: [
				{
					id: 1,
					name: 'Birds?',
					field_type: 'dropdown',
					is_required: true,
					custom_field_options: [
						{ value: 'Love' },
						{ value: 'Tepid Response' },
					]
				}
			],
			rules: [],

			name: 'Sage Grouse Breakfast',
			logo_url: 'path/to/image',
			description: 'in honor of the Sage Grouse',
			location: 'D.C.',

			time_scheduled: 1500000000,
			end_time_scheduled: 1500003600,
			timezone: 'America/New_York',

			unique_value: null,
			number_guests_allowed: 3,

			registration_count: 7,
			registration_cap: 16,

			whos_coming_enabled: true,
			waitlist_on: true
		},
		{
			id: 2,

			name: 'Keynote'
		}
	],
	primary_fields: [
		{
			id: 1,
			name: 'Favorite Bird',
			field_type: 'text',
			is_required: true
		},
		{
			id: 2,
			name: 'Favorite Bird Call',
			field_type: 'text',
			is_required: false
		}
	],
	shortcode: '012345679abcde',
    account_unique_field: 'ConstituentID'
});
