import React from 'react';
import { connect } from 'react-redux';

import { changeRoute } from '../../actions/';

const PrimaryRegistrationRevisit = (props) => (<article
id="primary-registration-revisit"
className="text-right">
	<button type="button" onClick={props.revisit}>
		&raquo; Return to Primary Registration
	</button>
</article>);


const mapStateToProps = state => state.primaryRegistration;

const mapDispatchToProps = dispatch => new Object({
	revisit: () => dispatch(changeRoute('PRIMARY_REGISTRATION'))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PrimaryRegistrationRevisit);
