import React from 'react';
import { connect } from 'react-redux';

import PrimaryRegistrationForm from './form/';
import PrimaryRegistrationRevisit from './revisit/';

const PrimaryRegistration = (props) => {
	if ( ! props.relevant) return null;

	return (<section id="primary-registration">
		{props.isActiveRoute
			? <PrimaryRegistrationForm />
			: <PrimaryRegistrationRevisit />
		}
	</section>);
}

const mapStateToProps = state => new Object({
	relevant: state.primaryRegistration.relevant,
	isActiveRoute: state.activeRoute === 'PRIMARY_REGISTRATION'
});

export default connect(mapStateToProps)(PrimaryRegistration);
