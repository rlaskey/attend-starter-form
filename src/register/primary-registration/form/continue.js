import React from 'react';
import { connect } from 'react-redux';

import {
	continuePrimaryRegistration
} from '../../actions/primary-registration';

const PrimaryRegistrationContinue = (props) => (<section className="text-right">
{props.errors.map(error => (<div key={error} className="text-danger">
	{error}
</div>))}

	<p>
		<button type="button" onClick={props.submit}>
			&raquo; Continue
		</button>
	</p>
</section>);


const mapStateToProps = state => state.primaryRegistration;

const mapDispatchToProps = dispatch => new Object({
	submit: () => dispatch(continuePrimaryRegistration())
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PrimaryRegistrationContinue);
