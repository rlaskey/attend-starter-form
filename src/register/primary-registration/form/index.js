import React from 'react';

import PrimaryRegistrationFieldset from './fieldset';
import PrimaryRegistrationContinue from './continue';

export default () => (<article id="primary-registration-form">
	<h2>Primary Registration</h2>

<blockquote>
	Enter in your information once, to allow these details to copy over into
	each Event you select.
</blockquote>

	<hr />

	<PrimaryRegistrationFieldset />
	<PrimaryRegistrationContinue />
</article>);
