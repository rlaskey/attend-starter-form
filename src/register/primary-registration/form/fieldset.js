import React from 'react';
import { connect } from 'react-redux';

import { Input } from '../../../core/forms';
import { updatePrimaryRegistration } from '../../actions/primary-registration';

const PrimaryRegistrationFieldset = (props) => (<fieldset>
{props.fields.map(field => (
	<Input key={field.field}
		name={field.name}
		field={field.field}
		onChange={props.onChange}
		required={field.is_required}
		value={props.data[field.field]} />
))}
</fieldset>);

const mapStateToProps = state => state.primaryRegistration;

const mapDispatchToProps = dispatch => new Object({
	onChange: (name, value) => {
		dispatch(updatePrimaryRegistration({ [name]: value }));
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PrimaryRegistrationFieldset);
