import '../vendor.js';

import React from 'react';
import { render } from 'react-dom';

import { HeroImage } from '../core/hero-image';

import css from '../style.css';

const Confirmation = () => (<div>
<HeroImage />

<main>
	<h1>Thank You for Registering!</h1>

	<p>We look forward to seeing you soon.</p>
</main>
</div>);

render(<Confirmation />, document.getElementById('root'));
